
: invert 0> if 0 else 1 then ;

26 output: spk
: beep
    msecs 22 +
    begin
        spk spk digr invert digw
        165 delay.us
        dup msecs - 0<
    until
    drop
    spk 0 digw
;

19 output: led
: blink
    "Aprieta cualquier tecla para salir." type
    begin
        beep
        led led digr invert digw
        2222 delay
        key?
    until
    " Fin." type
;

blink


19 output: led
: 500ms 500 delay-ms ;
: blink led on 500ms led off 500ms ;
: looper begin blink key? if "Finished" type exit then again ;
looper


19 output: led
: wait 500 delay-ms ;
: blink begin led on wait led off wait again ;
blink


19 output: led
: blink begin led on 666 delay-ms led off 666 delay-ms key? until ;
blink
